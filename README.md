# Iniziare ad usare la libreria per il collettore turnazione

Con questa libreria è possibile accedere alle informazioni:
- _Divisions_, le sedi attive
- _Shifters_, i dipendenti
- _TypeShift_, i tipi di turno
- _Shift_, la turnazione
- _Lock_, attivazione/disattivazione di lock 

sul [collettore della turnazione](https://gitlab.rm.ingv.it/csi/collettore-turnazione)

## Installazione e utilizzo

Fai riferimento al wiki circa l'installazione, l'autoload e l'utilizzo: 
https://gitlab.rm.ingv.it/diego.sorrentino/collettore-unico-library/-/wikis/Home


## Documentazione tecnica
Puoi trovare la documentazione tecnica, creata automaticamente tramite PHPDocumentor qui: 
http://diego.sorrentino.gitpages.rm.ingv.it/collettore-unico-library/

## Test
I test sono stati sviluppati usando PHPUnit


## Contributi
Se vuoi contribuire, fai una pull request
Per una migliore integrazione con l'attuale sviluppo, fai riferimento a Behavior Driven Development (BDD), Test Driven Development (TDD) e Calisthenic Programming.

## Autori e riconoscimenti
Diego Sorrentino, Istituto Nazionale di Geofisica e Vulcanologia, https://www.ingv.it/organizzazione/chi-siamo/personale/#922

## Licenza
GPL v3

## Stato dello sviluppo
Development in progress

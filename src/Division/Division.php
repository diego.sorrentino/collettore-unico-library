<?php
namespace CollettoreUnico\Division;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Division class
 *
 * @param string $proto		protocol to establish a connection to CollettoreTurnazione
 * @param string $fqdn		Full Qualified Domain Name of CollettoreTurnazione server
 * @param int	 $port		Listening port
 * @param string $auth		Auth code 
 */
class Division implements IteratorAggregate{
	const ERR_EMPTY_RESULT = -1;
	const ERR_EMPTY_ARRAY = -2;
	
	private string $authUrl = '';
	private string $searchUrl = '';

	private array $divisions;

	public function __construct(
			string	$proto,
			string	$fqdn,
			int	$port,
			string	$auth){

		$this->authUrl = sprintf('%s://%s:%d/%s', 
			$proto, $fqdn, $port, $auth);
	}

	public function __destruct(){ }

	/**
	 * Iterate over divisions found
	 * 
	 * @return array 	array of \CollettoreUnico\DataStructure\Shifter
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->divisions); }


	/**
	 * Search all divisions
	 * 
	 * @return int	Number of elems found
	 */
	public function fetch():int{
		$this->searchUrl = sprintf('%s/divisions', $this->authUrl);

		//removing http_proxy settings otherwise i dont reach the final server
		$proxy = getenv('http_proxy');	
		putenv("http_proxy");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->searchUrl,
			CURLOPT_HEADER 		=> false,
			//CURLOPT_VERBOSE		=> true,
			CURLOPT_RETURNTRANSFER  => true, //return data
			)
		);
		$downloadedData = curl_exec($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);

		//restoring http_proxy
		putenv(sprintf("http_proxy='%s'", $proxy));

		if( empty ($downloadedData) ){
			error_log(__METHOD__ . ' empty result');
			return self::ERR_EMPTY_RESULT;
		}

		$resArray = json_decode($downloadedData, true);
		if( empty($resArray) ){
			error_log(__METHOD__ . ' empty array');
			return self::ERR_EMPTY_ARRAY;
		}

		foreach($resArray as $data){
			$this->divisions[] = new Struct(
				$data['id'],
				$data['Denominazione'],
				$data['Intestazione'],
				$data['Citta'],
				$data['Sigla']
			);
		}

		return count($this->divisions);
	}

}
?>

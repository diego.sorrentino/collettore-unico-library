<?php
namespace CollettoreUnico\Division;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Data structure to handle Division
 *
 * @param int	 $zucchettiID	Zucchetti ID
 * @param string $name		Name
 * @param string $description	Description
 * @param string $city		City
 * @param string $acronim	Acronim
 */
class Struct implements IteratorAggregate{
	private int	$zucchettiID;
	private string	$name;
	private string	$description;
	private string	$city;
	private string	$acronim;

	public function __construct(
			int	$zucchettiID, 
			string 	$name, 
			string 	$description,
			string	$city,
			string	$acronim){

		$this->zucchettiID = $zucchettiID;
		$this->name = $name;
		$this->description = $description;
		$this->city = $city;
		$this->acronim = $acronim;
	}

	/**
	 * Get zucchettiID
	 *
	 * @return int ZucchettiID
	 */
	public function zucchettiID():int{return $this->zucchettiID; }

	/**
	 * Get name
	 *
	 * @return string name
	 */
	public function name():string{return $this->name; }

	/**
	 * Get description
	 *
	 * @return string description
	 */
	public function description():string{return $this->description; }

	/**
	 * Get city
	 *
	 * @return string city
	 */
	public function city():string{return $this->city; }

	/**
	 * Get acronim
	 *
	 * @return string acronim
	 */
	public function acronim():string{return $this->acronim; }

	/**
	 * Iterate over division info
	 * @return array 	array of all division info
	 */
	public function getIterator(): Traversable {
		return new ArrayIterator(array(
				'zucchettiID' => $this->zucchettiID(),
				'name' => $this->name(),
				'description' => $this->description(),
				'city' => $this->city(),
				'acronim' => $this->acronim(),
			)
		);
	}
}

?>

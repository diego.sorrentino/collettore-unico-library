<?php
namespace CollettoreUnico;

/**
 * Lock class
 *
 * @param string $proto		protocol to establish a connection to CollettoreTurnazione
 * @param string $fqdn		Full Qualified Domain Name of CollettoreTurnazione server
 * @param int	 $port		Listening port
 * @param string $auth		Auth code 
 */
class Lock {
	private const MAX_RETRIES = 10;
	private const SLEEP_SECS = 30;

	private const REQUEST_PERFORMED = 200;
	private const REQUEST_ALREADY_PERFORMED = 300; //this is an warning!
	private const RESOURCED_ARE_LOCKED = 400; 
	
	private string $authUrl = '';
	private string $lockUrl = '';

	private bool $resourceLocked = false;
	private array $response = array();

	/**
	 * on __construct automatically try to lock resources
	 */
	public function __construct(
			string	$proto,
			string	$fqdn,
			int	$port,
			string	$auth){

		$this->authUrl = sprintf('%s://%s:%d/%s', 
			$proto, $fqdn, $port, $auth);

		$this->acquire();
	}

	/**
	 * on __destruct automatically try to release resources
	 */
	public function __destruct(){ 
		$this->release();
	}

	/**
	 * Try to lock CollettoreUnico reources
	 * 
	 * @return bool	TRUE if lock is acquired, false otherwise
	 */
	public function acquire():bool{
		if ( $this->resourceLocked )
			return true;

		$this->lockUrl = sprintf("%s/lock/acquire", $this->authUrl);
		$retry = 0;
		while ( ! $this->fetch() && $retry++ < self::MAX_RETRIES )
			error_log(sprintf("[%s] Acquire Lock failure #%d", __METHOD__, $retry, self::SLEEP_SECS));
	
		$this->resourceLocked = true;
		return true;
	}

	/**
	 * Release CollettoreUnico resources
	 * 
	 * @return bool	TRUE if lock is release, false otherwise
	 */
	public function release():bool{
		if ( ! $this->resourceLocked )
			return true;

		$this->lockUrl = sprintf("%s/lock/release", $this->authUrl);

		while ( ! $this->fetch() && $retry++ < self::MAX_RETRIES )
			error_log(sprintf("[%s] Release Lock failure #%d, retrying in %d sec", __METHOD__, $retry, self::SLEEP_SECS));

		$this->resourceLocked = false;
		return true;
	}


	/**
	 * Connect to CollettoreUnico and request/release lock
	 *
	 * @return bool TRUE if request action is done and => $response property is set, false otherwise 	 
	 */
	private function fetch():bool{
		//removing http_proxy settings otherwise i dont reach the final server
		$proxy = getenv('http_proxy');	
		putenv("http_proxy");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->lockUrl,
			CURLOPT_HEADER 		=> false,
			//CURLOPT_VERBOSE		=> true,
			CURLOPT_RETURNTRANSFER  => true, //return data
			)
		);
		$downloadedData = curl_exec($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);

		//restoring http_proxy
		putenv(sprintf("http_proxy='%s'", $proxy));

		//should never happen...
		if( empty ($downloadedData) )
			throw new \UnexpectedValueException(sprintf("[%s] empty server response (%s)", __METHOD__, $downloadedData));

		$this->response = json_decode($downloadedData, true);
		//should never happen...
		if( empty($this->response) )
			throw new \UnexpectedValueException(sprintf("[%s] empty server response: \n%s", __METHOD__, print_r($this->response, true)));
		
		if( ! array_key_exists('code', $this->response) )
			throw new \UnexpectedValueException(sprintf("[%s] 'code' key not found in json response: %s", __METHOD__, print_r($this->response, true)));

		//lock acquired || released 
		if( self::REQUEST_PERFORMED == intval($this->response['code']) )
			return true;
		
		//just a warning (lock acquire request multiple times)
		if( self::REQUEST_ALREADY_PERFORMED == intval($this->response['code']) ){
			error_log(sprintf("[%s] WARNING: Acquire/Release lock error, code %d", __METHOD__, self::REQUEST_ALREADY_PERFORMED));
			return true;
		}

		//the last returning code, locked already acquired by another user...
		//you have to wait :-)
		//if( self::RESOURCED_ARE_LOCKED  == $this->response['code'] )
		$sleepTime =  array_key_exists('expire_in_secs', $this->response)
			? intval($this->response['expire_in_secs']) + 1
			: self::SLEEP_SECS;

		error_log( sprintf('Resources locked, retry in %d seconds', $sleepTime) );
		sleep($sleepTime);

		return false; //returning false so request will be fetched again
	}

}
?>

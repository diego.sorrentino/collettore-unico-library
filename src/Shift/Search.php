<?php
namespace CollettoreUnico\Shift;

/**
 * Shifts Search class
 *
 * @param string $proto		protocol to establish a connection to CollettoreTurnazione
 * @param string $fqdn		Full Qualified Domain Name of CollettoreTurnazione server
 * @param int	 $port		Listening port
 * @param string $auth		Auth code 
 */
class Search extends Shift {

	private \Datetime $startDate;
	private \Datetime $stopDate;
	private array $idTypeShifts = array();
	private array $idShifters = array();
	private array $idGroups = array();
	private bool $history = false;

	private bool $swapsAddedInLate = false;

	/**
	 * Add filters by swap shifts added in late
	 */
	public function addFilterSwapShiftsAddedInLate():void { $this->swapsAddedInLate = true; }

	/**
	 * Add filters by start date
	 *
	 * @param 	\Datetime 	Valid \Datetime. Default value is current day
	 */
	public function addFilterByStartDate(\Datetime $startDate):void { $this->startDate = $startDate; }

	/**
	 * Add filters by stop date
	 *
	 * @param 	\Datetime 	Valid \Datetime. Default value is current day
	 */
	public function addFilterByStopDate(\Datetime $stopDate):void { $this->stopDate = $stopDate; }

	/**
	 * Add filters by TypeShifts Zucchetti ID .
	 * Calling this method you can add several ID to search.
	 *
	 * @param 	int 	idTypeShift in Zucchetti
	 */
	public function addFilterByIdTypeShifts(int $idTypeShifts):void { $this->idTypeShifts[] = $idTypeShifts; }

	/**
	 * Add filters by Shifter Zucchetti ID.
	 * Calling this method you can add several ID to search.
	 *
	 * @param 	string 	 idShifter in Zucchetti (12 chars, must match /[A-Z]{6}[0-9]{6}/)
	 */
	public function addFilterByIdShifters(string $idShifter):void { $this->idShifters[] = $idShifter; }

	/**
	 * Add filters by internal idGroup (division)
	 * Calling this method you can add several ID to search.
	 *
	 * @param 	int     idGroup in CollettoreUnico
	 */
	public function addFilterByIdGroups(int $idGroup):void { $this->idGroups[] = $idGroup; }

	/**
	 * Add filters on historical data (search swap shifts changes)
	 *
	 * @param 	bool	Search all swap shifts changes
	 */
	public function addFilterByHistory(bool $history):void { $this->history = $history; }

	/**
	 * Set filters to select needed data
	 *
	 * @return string json encoded filters
	 */
	private function setFilters():string { 
		
		if ( ! isset($this->startDate) )
			$this->startDate = new \Datetime();

		if ( ! isset($this->stopDate) )
			$this->stopDate = new \Datetime();

		$filters['data_da'] = $this->startDate->format('Y-m-d');
		$filters['data_a'] = $this->stopDate->format('Y-m-d');
		$filters['details'] = 1;
		$filters['history'] = intval($this->history);

		if ( ! empty ($this->idTypeShifts) )
			$filters['id_turno'] = $this->idTypeShifts;

		if ( ! empty ($this->idShifters) )
			$filters['id_turnista'] = $this->idShifters;

		if ( ! empty ($this->idGroups) )
			$filters['gruppo'] = $this->idGroups;

		return json_encode($filters);
	}
	
	/**
	 * Connect to CollettoreUnico and do searchs
	 *
	 * @return int	Number of elems found
	 */
	public function fetch():int{
		$this->searchUrl = sprintf('%s/shifts/%s', 
			$this->authUrl,
			$this->swapsAddedInLate ? 'requestInLate' : 'request');

		$jsonSearchSettings = $this->swapsAddedInLate ? '{}' : $this->setFilters();

		//removing http_proxy settings otherwise i dont reach the final server
		$proxy = getenv('http_proxy');	
		putenv("http_proxy");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->searchUrl,
			CURLOPT_HEADER 		=> false,
			CURLOPT_HTTPHEADER 	=> array('Content-Type: application/json'),
			CURLOPT_TIMEOUT 	=> 10000,
			CURLOPT_POST 		=> 1,
			CURLOPT_POSTFIELDS 	=> $jsonSearchSettings,
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_FAILONERROR     => true,
			//CURLOPT_VERBOSE		=> true,
			)
		);
		$downloadedData = curl_exec($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);

		//restoring http_proxy
		putenv(sprintf("http_proxy='%s'", $proxy));

		if( empty ($downloadedData) ){
			error_log(__METHOD__ . ' empty result');
			return self::ERR_EMPTY_RESULT;
		}

		$res = json_decode($downloadedData, true);
		if( empty($res) ){
			error_log(__METHOD__ . ' empty array');
			return self::ERR_EMPTY_ARRAY;
		}
		
		foreach($res as $shift)
			$this->data[] = new Struct(
				\DateTime::createFromFormat('Y-m-d', $shift['data']),
				$shift['id_turno'],
				$shift['id_turnista'],
				$shift['IDEMPLOY'],
				$shift['flnnumber'],
				new \DateTime($shift['when_added']),
				$shift['name'],
				$shift['id_gruppo']);
				
		return count($this->data);
	}
}
?>

<?php
namespace CollettoreUnico\Shift;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Shift class
 * just implements __constructor, __destructor and iterator
 *
 * @param string $proto		protocol to establish a connection to CollettoreTurnazione
 * @param string $fqdn		Full Qualified Domain Name of CollettoreTurnazione server
 * @param int	 $port		Listening port
 * @param string $auth		Auth code 
 */
class Shift implements IteratorAggregate{
	public const ERR_EMPTY_RESULT = -1;
	public const ERR_EMPTY_ARRAY = -2;
	public const ERR_INVALID_DATA = -3;
	public const OK_UPDATE = 1;
	
	protected string $authUrl = '';
	protected string $searchUrl = '';

	protected array $data;

	public function __construct(
			string	$proto,
			string	$fqdn,
			int	$port,
			string	$auth){

		$this->authUrl = sprintf('%s://%s:%d/%s', 
			$proto, $fqdn, $port, $auth);

		$this->data = array();
	}

	public function __destruct(){ }

	/**
	 * Iterate over shifts found
	 * 
	 * @return  array found Data
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->data); }

}
?>

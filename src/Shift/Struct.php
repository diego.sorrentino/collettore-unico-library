<?php
namespace CollettoreUnico\Shift;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Data structure to handle Shift
 *
 * @param \Datetime $data	date
 * @param int	 $idTypeShift	zucchetti ID shift 
 * @param string $idSubject	zucchetti ID shifter, in [A-Z]{6}[0-9]{6}
 * @param int	 $idEmployee	zucchetti ID employee (id workcontract)
 * @param int	 $flnnumber	flow number (1 is the actual shift)
 * @param \Datetime $whenAdded		when this shift was added
 * @param string $name		user who add this record
 * @param int	$groupId	user group who add this recorduser who add this record
 */
class Struct implements IteratorAggregate{
	private \Datetime $data;
	private int $idTypeShift;
	private string $idSubject;
	private int $idEmployee;
	private int $flnnumber;
	private \Datetime $whenAdded;
	private string $name;
	private int $groupId;

	public function __construct( \Datetime $data, int $idTypeShift, string $idSubject, int $idEmployee,
		int $flnnumber, \Datetime $whenAdded, string $name, int $groupId){

		$this->data = $data;
		$this->idTypeShift = $idTypeShift;
		$this->idSubject = $idSubject;
		$this->idEmployee = $idEmployee;
		$this->flnnumber = $flnnumber;
		$this->whenAdded = $whenAdded;
		$this->name = $name;
		$this->groupId = $groupId;
	}

	/**
	 * Get data
	 *
	 * @return string data in YYYY-MM-DD format
	 */
	public function data():string{return $this->data->format('Y-m-d'); }

	/**
	 * Get data zucchetti format
	 *
	 * @return string data in dd/mm/yyyy format
	 */
	public function dataZucchettiFormat():string{return $this->data->format('d/m/Y'); }

	/**
	 * Get idTypeShift
	 *
	 * @return int idTypeShift according to zucchetti ID
	 */
	public function idTypeShift():int{return $this->idTypeShift; }

	/**
	 * Get idSubject
	 *
	 * @return string idSubject in [A-Z]{6}[0-9]{6} format
	 */
	public function idSubject():string{return $this->idSubject; }

	/**
	 * Get idEmployee
	 *
	 * @return int idEmployee according to zucchetti ID
	 */
	public function idEmployee():int{return $this->idEmployee; }

	/**
	 * Get flow number (1 is the actual shift)
	 *
	 * @return int flow number (1 is the actual shift)
	 */
	public function flnnumber():int{return $this->flnnumber; }

	/**
	 * Get whenAdded
	 *
	 * @return \Datetime whenAdded in YYYY-MM-DD format
	 */
	public function whenAdded():\Datetime{return $this->whenAdded; }

	/**
	 * Get username who add this row
	 *
	 * @return string username who add this row
	 */
	public function name():string{return $this->name; }

	/**
	 * Get groupId of the username who add this row
	 *
	 * @return int groupId of the username who add this row
	 */
	public function groupId():int{return $this->groupId; }

	/**
	 * Iterate over shift info
	 * @return array 	array of all shift info
	 */
	public function getIterator(): Traversable {
		return new ArrayIterator(array(
				'data' => $this->data(),
				'dataZucchettiFormat' => $this->dataZucchettiFormat(),
				'idTypeShift' => $this->idTypeShift(),
				'idSubject' => $this->idSubject(),
				'idEmployee' => $this->idEmployee(),
				'flnnumber' => $this->flnnumber(),
				'whenAdded' => $this->whenAdded(),
				'name' => $this->name(),
				'groupId' => $this->groupId(),
			)
		);
	}
}

?>

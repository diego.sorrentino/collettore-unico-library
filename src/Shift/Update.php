<?php
namespace CollettoreUnico\Shift;

/**
 * Shifts Update class
 *
 * @param string $proto		protocol to establish a connection to CollettoreTurnazione
 * @param string $fqdn		Full Qualified Domain Name of CollettoreTurnazione server
 * @param int	 $port		Listening port
 * @param string $auth		Auth code 
 */
class Update extends Shift {


	/**
	 * Send shifts to collettore unico 
	 * this method receive an array an send a valid json to collettore unico
	 * @param	array	Array of ('data' => 'yyyy-mm-dd', "id_turno": zucchetti_IDTURN, "id_turnista": "zucchetti_IDSUBJECT")
	 *
	 * @return int 	Results code (see constants)
	 */
	public function update(array $shifts):int{
		//removing http_proxy settings otherwise i dont reach the final server
		$proxy = getenv('http_proxy');	
		putenv("http_proxy");

		if ( empty ($shifts ) )
			return self::ERR_EMPTY_ARRAY;

		if ( ! $this->shiftsAreValid($shifts) )
			return self::ERR_INVALID_DATA;

		$this->searchUrl = sprintf('%s/shifts', $this->authUrl);

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->searchUrl,
			CURLOPT_HEADER 		=> false,
			CURLOPT_HTTPHEADER 	=> array('Content-Type: application/json'),
			CURLOPT_TIMEOUT 	=> 10000,
			CURLOPT_POST 		=> 1,
			CURLOPT_POSTFIELDS 	=> json_encode($shifts),
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_FAILONERROR     => true,
			//CURLOPT_VERBOSE		=> true,
			)
		);
		$downloadedData = curl_exec($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);

		//restoring http_proxy
		putenv(sprintf("http_proxy='%s'", $proxy));

		if( empty ($downloadedData) ){
			error_log(__METHOD__ . ' empty result');
			return self::ERR_EMPTY_RESULT;
		}

		$this->data = json_decode($downloadedData, true);
		if( empty($this->data) ){
			error_log(__METHOD__ . ' empty array');
			return self::ERR_EMPTY_ARRAY;
		}

		return self::OK_UPDATE;
	}

	/**
	 * Fetch server response
	 *
	 * @return array	( [status] => %s [code] => %d [idlog] => %d [numRows] => %d)
	 */
	public function response():array{ return $this->data; }

	/**
	 * Checks if array of shifts are all valid
	 *
	 * @param	array 	( array	( [status] => %s [code] => %d [idlog] => %d [numRows] => %d) )
	 * @return	bool 	True, shifts are ALL valid, False if at-least one is invalid (and stop checking the followings)
	 */
	private function shiftsAreValid(array $shifts):bool{
		$validKeys = array('data', 'id_turno', 'id_turnista');
		foreach($shifts as $shift){
			if ( ! is_array($shift) ){
				error_log(sprintf("[%s] <%s> is not a valid array",
					__METHOD__, 
					print_r($shift, true)
				));
				return false;
			}

			if ( ! $this->keysAreValids($validKeys, array_keys($shift)) ){
				error_log(sprintf("[%s] shifts keys (%s) doesnt match", 
					__METHOD__, 
					implode(', ', array_keys($shift))
				));
				return false;
			}

			if ( ! $this->dateIsValid($shift['data']) ){
				error_log(sprintf("[%s] Date %s is invalid", __METHOD__, $shift['data']));
				return false;
			}

			if ( ! $this->idShiftIsValid($shift['id_turno']) ){
				error_log(sprintf("[%s] ID shift %s is invalid", __METHOD__, $shift['id_turno']));
				return false;
			}

			if ( ! $this->idShifterIsValid($shift['id_turnista']) ){
				error_log(sprintf("[%s] ID shifter %s is invalid", __METHOD__, $shift['id_turnista']));
				return false;
			}
		}

		return true;
	}

	/**
	 * Checks if array keys are all valid AND all must be present
	 *
	 * @param	array 	$validKeys array of valid keys name
	 * @param	array 	$receivedKeys array of keys to check
	 * @return	bool 	True, keys are valid, False otherwise
	 */
	private function keysAreValids(array $validKeys, array $receivedKeys):bool{
		return empty(array_diff($validKeys, $receivedKeys)) && empty(array_diff($receivedKeys, $validKeys));
	}

	/**
	 * Checks if date is valid
	 *
	 * @param	string 	$date	date to check in format YYYY-MM-DD
	 * @return	bool 	True, date is valid, False otherwise
	 */
	private function dateIsValid(string $day):bool{
		return preg_match('!^20(\d\d)-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$!', $day);
	}

	/**
	 * Checks if idShift format is valid
	 *
	 * @param	int	$idShift  Check only if idShift format is valud (is numeric and is integer)
	 * @return	bool 	True, date is valid, False otherwise
	 */
	private function idShiftIsValid(int $idShift):bool { 
		return is_numeric($idShift) && is_int($idShift); 
	}

	/**
	 * Checks if idShifter format is valid
	 *
	 * @param	string	$idShifter  Check only if idShifter is valid (match this regexp /^[A-Z]{6}[0-9]{6}/)
	 * @return	bool 	True, date is valid, False otherwise
	 */
	private function idShifterIsValid(string $idShifter):bool { 
		return preg_match('!^[A-Z]{6}[0-9]{6}!', $idShifter);
	}
	
}
?>

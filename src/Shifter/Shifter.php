<?php
namespace CollettoreUnico\Shifter;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Shifter class
 *
 * @param string $proto		protocol to establish a connection to CollettoreTurnazione
 * @param string $fqdn		Full Qualified Domain Name of CollettoreTurnazione server
 * @param int	 $port		Listening port
 * @param string $auth		Auth code 
 */
class Shifter implements IteratorAggregate{
	const ERR_EMPTY_RESULT = -1;
	const ERR_EMPTY_ARRAY = -2;
	
	private string $authUrl = '';
	private string $searchUrl = '';

	private array $shifters;
	private ?Struct $shifter;
	

	public function __construct(
			string	$proto,
			string	$fqdn,
			int	$port,
			string	$auth){

		$this->authUrl = sprintf('%s://%s:%d/%s', 
			$proto, $fqdn, $port, $auth);
	}

	public function __destruct(){ }

	/**
	 * Get the unique elem found
	 *
	 * @return array 	array of \CollettoreUnico\DataStructure\Shifter
	 */
	public function shifter():?Struct{ return $this->shifter; }

	/**
	 * Iterate over shifters found
	 *
	 * @return array 	array of \CollettoreUnico\DataStructure\Shifter
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->shifters); }

	/**
	 * Search usr by Zucchetti ID
	 *
	 * @return int	Number of elems found
	 */
	public function filterByZucchettiID(string $zucchettiID):int { 
		$this->searchUrl = sprintf('%s/employees/IDSUBJECT/%s', $this->authUrl, $zucchettiID);
		return $this->fetch();
	}

	/**
	 * Search usr by email
	 *
	 * @return int	Number of elems found
	 */
	public function filterByEmail(string $email):int { 
		$this->searchUrl = sprintf('%s/employees/EMAIL/%s', $this->authUrl, $email);
		return $this->fetch();
	}

	/**
	 * Search usr by fiscal code
	 *
	 * @return int	Number of elems found
	 */
	public function filterByFiscalCode(string $fiscalCode):int { 
		$this->searchUrl = sprintf('%s/employees/IDIDENTIFP/%s', $this->authUrl, $fiscalCode);
		return $this->fetch();
	}

	/**
	 * Request a employees table resync with Zucchetti, in Collettore Unico
	 * NOTE: before /employees/resync you have to lock the remote resources ( /lock/acquire )
	 */
	public function resyncRemoteData():void{ 
		$this->searchUrl = sprintf('%s/employees/resync', $this->authUrl);
		$this->fetch();
	}


	/**
	 * Search all active users (not layoff)
	 *
	 * @return int	Number of elems found
	 */
	public function filterByActive():int { 
		$this->searchUrl = sprintf('%s/employees/active', $this->authUrl);
		return $this->fetch();
	}

	/**
	 * Search all layoff users 
	 *
	 * @return int	Number of elems found
	 */
	public function filterByLayOff():int { 
		$this->searchUrl = sprintf('%s/employees/layoff', $this->authUrl);
		return $this->fetch();
	}

	/**
	 * Search all users by division
	 *
	 * @return int	Number of elems found
	 */
	public function filterAllUsrByDivision(int $idDivision):int { 
		$this->searchUrl = sprintf('%s/employees/division/%d', $this->authUrl, $idDivision);
		return $this->fetch();
	}

	/**
	 * Search all active users by division
	 *
	 * @return int	Number of elems found
	 */
	public function filterActiveUsrByDivision(int $idDivision):int { 
		$this->searchUrl = sprintf('%s/employees/division/%d/active', $this->authUrl, $idDivision);
		return $this->fetch();
	}

	/**
	 * Search all layoff users by division
	 *
	 * @return int	Number of elems found
	 */
	public function filterLayoffUsrByDivision(int $idDivision):int { 
		$this->searchUrl = sprintf('%s/employees/division/%d/layoff', $this->authUrl, $idDivision);
		return $this->fetch();
	}
	
	/**
	 * Connect to CollettoreUnico and do searchs
	 *
	 * @return int	Number of elems found
	 */
	private function fetch():int{
		//removing http_proxy settings otherwise i dont reach the final server
		$proxy = getenv('http_proxy');	
		putenv("http_proxy");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->searchUrl,
			CURLOPT_HEADER 		=> false,
			//CURLOPT_VERBOSE		=> true,
			CURLOPT_RETURNTRANSFER  => true, //return data
			)
		);
		$downloadedData = curl_exec($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);

		//restoring http_proxy
		putenv(sprintf("http_proxy='%s'", $proxy));

		if( empty ($downloadedData) ){
			error_log(__METHOD__ . ' empty result');
			return self::ERR_EMPTY_RESULT;
		}

		$resArray = json_decode($downloadedData, true);
		if( empty($resArray) ){
			error_log(__METHOD__ . ' empty array');
			return self::ERR_EMPTY_ARRAY;
		}

		foreach($resArray as $data){
			$this->shifters[] = new Struct(
				$data['IDSUBJECT'],
				$data['ANNAME'],
				$data['ANSURNAM'],
				$data['IDIDENTIFP'],
				new \DateTime($data['DTASSUMPT']),
				empty( $data['DTLAYOFF'] ) ? null : new \DateTime($data['DTLAYOFF']),
				$data['IDUNIT'],
				$data['ANEMAIL']
			);
		}

		$this->shifter = ( 1 == count($this->shifters) ) ? $this->shifters[0] : null;

		return count($this->shifters);
	}

}
?>

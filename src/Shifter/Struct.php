<?php
namespace CollettoreUnico\Shifter;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Data structure to handle Shifter
 *
 * @param string 	$zucchettiID	Zucchetti ID
 * @param string 	$name		Name
 * @param string 	$surname	Surname
 * @param string 	$fiscalCode	Fiscal code
 * @param \DateTime	$hireDate	hireDate
 * @param \DateTime	$layoffDate	(nullable) layoffDate
 * @param int		$idUnit		id Unit
 * @param string 	$email		Email
 */
class Struct implements IteratorAggregate{
	private string	$zucchettiID;
	private string	$name;
	private string	$surname;
	private string	$fiscalCode;
	private \DateTime $hireDate;
	private ?\DateTime $layoffDate;
	private int	$idUnit;
	private string	$email;

	public function __construct(
			string 	$zucchettiID, 
			string 	$name, 
			string 	$surname,
			string	$fiscalCode,
			\DateTime $hireDate,
			?\DateTime $layoffDate,
			int	$idUnit,
			string	$email){

		$this->zucchettiID = $zucchettiID;
		$this->name = $name;
		$this->surname = $surname;
		$this->fiscalCode = $fiscalCode;
		$this->hireDate = $hireDate;
		$this->layoffDate = $layoffDate;
		$this->idUnit = $idUnit;
		$this->email = $email;
	}

	/**
	 * Get zucchettiID
	 *
	 * @return string ZucchettiID
	 */
	public function zucchettiID():string{return $this->zucchettiID; }

	/**
	 * Get shifter name
	 *
	 * @return string name
	 */
	public function name():string{return $this->name; }

	/**
	 * Get shifter surname
	 *
	 * @return string surname
	 */
	public function surname():string{return $this->surname; }

	/**
	 * Get fiscalCode
	 *
	 * @return string fiscalCode
	 */
	public function fiscalCode():string{return $this->fiscalCode; }

	/**
	 * Get hireDate
	 *
	 * @return \DateTime hireDate
	 */
	public function hireDate():\DateTime{return $this->hireDate; }

	/**
	 * Get layoffDate
	 *
	 * @return \DateTime (nullable) layoffDate
	 */
	public function layoffDate():?\DateTime{return $this->layoffDate; }

	/**
	 * Get idUnit
	 *
	 * @return int idUnit
	 */
	public function idUnit():int{return $this->idUnit; }

	/**
	 * Get email
	 *
	 * @return string email
	 */
	public function email():string{return $this->email; }

	/**
	 * Iterate over shifter info
	 * @return array 	array of all shifter info
	 */
	public function getIterator(): Traversable {
		return new ArrayIterator(array(
				'zucchettiID' => $this->zucchettiID(),
				'name' => $this->name(),
				'surname' => $this->surname(),
				'fiscalCode' => $this->fiscalCode(),
				'hireDate' => $this->hireDate(),
				'layoffDate' => $this->layoffDate(),
				'idUnit' => $this->idUnit(),
				'email' => $this->email(),
			)
		);
	}
}

?>

<?php
namespace CollettoreUnico\TypeShift;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Data structure to handle Division
 *
 * @param int	 $zucchettiID	Zucchetti ID
 * @param string $description	Description
 * @param int	 $groupID	ID group
 * @param string $groupDescription	Group description
 */
class Struct implements IteratorAggregate{
	private const DEFAULT_COLOR = '#000000';
	private const DEFAULT_START_TIME = '00:00:00';
	private const DEFAULT_END_TIME = '23:59:59';

	private int	$zucchettiID;
	private string	$description;
	private string	$label;
	private string	$color;
	private string	$startTime;
	private string	$endTime;
	private bool	$endNextDay;

	private string	$aggregatorName;
	private int	$groupID;
	private string	$groupDescription;

	private string	$startTimeWithoutOverlap;
	private	int	$startTimeOverlap; //in minutes

	private string	$endTimeWithoutOverlap;
	private	int	$endTimeOverlap; //in minutes

	public function __construct(
			int	$zucchettiID, 
			string 	$description,
			string 	$label,
			string 	$color,
			string 	$startTime,
			string 	$endTime,
			bool	$endNextDay,
			string	$aggregatorName,
			int	$groupID,
			string	$groupDescription){

		$this->zucchettiID = $zucchettiID;
		$this->description = $description;
		$this->label = $label;
		$this->color = $this->colorIsValid($color) ? $color : self::DEFAULT_COLOR;
		$this->startTime = $this->timeIsValid($startTime) ? $startTime : self::DEFAULT_START_TIME;
		$this->endTime = $this->timeIsValid($endTime) ? $endTime : self::DEFAULT_END_TIME;
		$this->endNextDay = $endNextDay;

		$this->aggregatorName = $aggregatorName;

		$this->groupID = $groupID;
		$this->groupDescription = $groupDescription;

		$this->startTimeWithoutOverlap = $this->startTime;
		$this->startTimeOverlap = 0;

		$this->endTimeWithoutOverlap = $this->endTime;
		$this->endTimeOverlap = 0;
		
		$this->findStartTimeOverlap();
		$this->findEndTimeOverlap();
	}

	/**
	 * Get zucchettiID
	 *
	 * @return int ZucchettiID
	 */
	public function zucchettiID():int{return $this->zucchettiID; }

	/**
	 * Get description
	 *
	 * @return string description
	 */
	public function description():string{return $this->description; }

	/**
	 * Get label
	 *
	 * @return string label
	 */
	public function label():string{return $this->label; }

	/**
	 * Get (bg)color
	 *
	 * @return string color assigned on zucchetti
	 */
	public function color():string{return $this->color; }

	/**
	 * Get StartTime 
	 *
	 * @return string start time exactly as store in zucchetti
	 */
	public function startTime():string{return $this->startTime; }

	/**
	 * Get StartTime without overlap time
	 *
	 * @return string start time without overlap time
	 */
	public function startTimeWithoutOverlap():string{return $this->startTimeWithoutOverlap; }

	/**
	 * Get StartTime overlap
	 *
	 * @return string start time overlap (in minutes)
	 */
	public function startTimeOverlap():int{return $this->startTimeOverlap; }

	/**
	 * Get EndTime 
	 *
	 * @return string end time of shift exactly as store in zucchetti
	 */
	public function endTime():string{return $this->endTime; }

	/**
	 * Get EndTime without overlap time
	 *
	 * @return string end time without overlap time
	 */
	public function endTimeWithoutOverlap():string{return $this->endTimeWithoutOverlap; }

	/**
	 * Get EndTime overlap
	 *
	 * @return string end time overlap (in minutes)
	 */
	public function endTimeOverlap():int{return $this->endTimeOverlap; }

	/**
	 * Get End next day
	 *
	 * @return bool TRUE if the shift end the following day, false otherwise
	 */
	public function endNextDay():bool{return $this->endNextDay; }

	/**
	 * Get aggregator name 
	 *
	 * @return string aggregator name
	 */
	public function aggregatorName():string{return $this->aggregatorName; }

	/**
	 * Get id group
	 *
	 * @return int group id
	 */
	public function groupID():int{return $this->groupID; }

	/**
	 * Get group description
	 *
	 * @return string group description
	 */
	public function groupDescription():string{return $this->groupDescription; }

	/**
	 * Iterate over type shifts info
	 * @return array 	array of all type shifts info
	 */
	public function getIterator(): Traversable {
		return new ArrayIterator(array(
				'zucchettiID' => $this->zucchettiID(),
				'description' => $this->description(),
				'label' => $this->label(),
				'color' => $this->color(),
				'startTime' => $this->startTime(),
				'startTimeWithoutOverlap' => $this->startTimeWithoutOverlap(),
				'startTimeOverlap' => $this->startTimeOverlap(),
				'endTime' => $this->endTime(),
				'endTimeWithoutOverlap' => $this->endTimeWithoutOverlap(),
				'endTimeOverlap' => $this->endTimeOverlap(),
				'aggregatorName' => $this->aggregatorName(),
				'groupID' => $this->groupID(),
				'groupDescription' => $this->groupDescription(),
			)
		);
	}
	
	
	/**
	 * Check if HEX color is valid
	 * @param  string $color color to check in #000000 format
	 *
	 * @return bool TRUE if is valid, FALSE otherwise
	 */
	private function colorIsValid(string $color):bool{ return preg_match('/^#[0-9A-F]{6}$/', strtoupper($color)); }

	/**
	 * Check if time is valid
	 * @param  string $time time in HH:mm:ss format 
	 *
	 * @return bool TRUE if is valid, FALSE otherwise
	 */
	private function timeIsValid(string $time):bool{ return preg_match('/^\d\d:\d\d:\d\d/', $time); }

	/**
	 * Check if is an 'On-call' shift
	 * @param  string $description Type shift group description
	 *
	 * @return bool TRUE if is valid, FALSE otherwise
	 */
	private function isOnCall(string $description):bool{ return preg_match('/reperibil/', strtolower($description)); }
	

	/**
	 * Find start time overlap and divide from startTime (setting $startTimeWithoutOverlap and $startTimeOverlap)
	 * @param  string $time time in HH:mm:ss format 
	 */
	private function findStartTimeOverlap():void{ 
		if( $this->isOnCall($this->groupDescription) ){
			return;
		}

		if( preg_match('/sala operativa roma/', strtolower($this->groupDescription)) ){
			$this->setStartTimeOverLapForRoma(strtolower($this->description));
			return;
		}

		if( preg_match('/sala operativa o[ve]/', strtolower($this->groupDescription)) ){
			//O[VE} dont overlap at startTime
			return;
		}
	}

	/**
	 * Set startTime overlap for Roma
	 */
	private function setStartTimeOverLapForRoma(string $description):void{ 
		if( preg_match('/mattin[oa]/', $description) )
			return;

		if( preg_match('/pomeriggio/', $description) ){
			$this->startTimeWithoutOverlap = '13:00:00';
			$this->startTimeOverlap = 15;
			return;
		}

		if( preg_match('/notte/', $description) ){
			$this->startTimeWithoutOverlap = '23:00:00';
			$this->startTimeOverlap = 15;
			return;
		}
	}

	/**
	 * Find end time overlap and divide from endTime (setting $endTimeWithoutOverlap and $endTimeOverlap)
	 * @param  string $time time in HH:mm:ss format 
	 */
	private function findEndTimeOverlap():void{ 

		if( $this->isOnCall($this->groupDescription) ){
			return;
		}

		if( preg_match('/sala operativa roma/', strtolower($this->groupDescription)) ){
			$this->setEndTimeOverLapForRoma(strtolower($this->description));
			return;
		}

		if( preg_match('/sala operativa ov/', strtolower($this->groupDescription)) ){
			$this->setEndTimeOverLapForOV(strtolower($this->description));
			return;
		}

		if( preg_match('/sala operativa oe/', strtolower($this->groupDescription)) ){
			$this->setEndTimeOverLapForOE(strtolower($this->description));
			//O[VE} dont overlap at endTime
			return;
		}
	}

	/**
	 * Set endTime overlap for Roma
	 */
	private function setEndTimeOverLapForRoma(string $description):void{ 
		if( preg_match('/mattin[oa]/', $description) )
			return;

		if( preg_match('/pomeriggio/', $description) ){
			$this->endTimeWithoutOverlap = '23:00:00';
			$this->endTimeOverlap = 15;
			return;
		}

		if( preg_match('/notte/', $description) ){
			$this->endTimeWithoutOverlap = '08:00:00';
			$this->endTimeOverlap = 15;
			return;
		}
	}

	/**
	 * Set endTime overlap for OV
	 */
	private function setEndTimeOverLapForOV(string $description):void{ 
		if( preg_match('/mattin[oa]/', $description) ){
			$this->endTimeWithoutOverlap = '14:15:00';
			$this->endTimeOverlap = 15;
			return;
		}

		if( preg_match('/pomeriggio/', $description) ){
			$this->endTimeWithoutOverlap = '20:30:00';
			$this->endTimeOverlap = 15;
			return;
		}

		if( preg_match('/notte/', $description) ){
			$this->endTimeWithoutOverlap = '08:00:00';
			$this->endTimeOverlap = 30;
			return;
		}
	}

	/**
	 * Set endTime overlap for OE
	 */
	private function setEndTimeOverLapForOE(string $description):void{ 
		if( preg_match('/mattin[oa]/', $description) ){
			$this->endTimeWithoutOverlap = '16:00:00';
			$this->endTimeOverlap = 20;
			return;
		}

		if( preg_match('/pomeriggio/', $description) ){
			$this->endTimeWithoutOverlap = '00:00:00';
			$this->endTimeOverlap = 20;
			return;
		}

		if( preg_match('/notte/', $description) ){
			$this->endTimeWithoutOverlap = '08:20:00';
			$this->endTimeOverlap = 20;
			return;
		}
	}
}

?>

<?php
namespace CollettoreUnico\TypeShift;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * TypeShift class
 *
 * @param string $proto		protocol to establish a connection to CollettoreTurnazione
 * @param string $fqdn		Full Qualified Domain Name of CollettoreTurnazione server
 * @param int	 $port		Listening port
 * @param string $auth		Auth code 
 */
class TypeShift implements IteratorAggregate{
	const ERR_EMPTY_RESULT = -1;
	const ERR_EMPTY_ARRAY = -2;
	
	private string $authUrl = '';
	private string $searchUrl = '';

	private array $typeShifts;

	public function __construct(
			string	$proto,
			string	$fqdn,
			int	$port,
			string	$auth){

		$this->authUrl = sprintf('%s://%s:%d/%s', 
			$proto, $fqdn, $port, $auth);
	}

	public function __destruct(){ }

	/**
	 * Iterate over type shifts found
	 * 
	 * @return array 	array of \CollettoreUnico\DataStructure\TypeShift
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->typeShifts); }
	
	/**
	 * Search all type shifts
	 * 
	 * @return int	Number of elems found
	 */
	public function fetch():int{
		$this->searchUrl = sprintf('%s/typeshifts', $this->authUrl);

		//removing http_proxy settings otherwise i dont reach the final server
		$proxy = getenv('http_proxy');	
		putenv("http_proxy");

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
			CURLOPT_URL 		=> $this->searchUrl,
			CURLOPT_HEADER 		=> false,
			//CURLOPT_VERBOSE		=> true,
			CURLOPT_RETURNTRANSFER  => true, //return data
			)
		);
		$downloadedData = curl_exec($curlSession);
		$errorInfo = curl_error($curlSession);
		curl_close($curlSession);

		//restoring http_proxy
		putenv(sprintf("http_proxy='%s'", $proxy));

		if( empty ($downloadedData) ){
			error_log(__METHOD__ . ' empty result');
			return self::ERR_EMPTY_RESULT;
		}

		$resArray = json_decode($downloadedData, true);
		if( empty($resArray) ){
			error_log(__METHOD__ . ' empty array');
			return self::ERR_EMPTY_ARRAY;
		}

		foreach($resArray as $data){
			$this->typeShifts[] = new Struct(
				intval($data['id_turno']),
				$data['descrizione'],
				$data['label'],
				$data['colore'],
				$data['orario_inizio_turno'],
				$data['orario_fine_turno'],
				(bool)$data['finisce_giorno_dopo'],
				$data['nome_aggregatore'],
				intval($data['gruppo']),
				$data['descrizionegruppo']
			);
		}

		return count($this->typeShifts);
	}

}
?>

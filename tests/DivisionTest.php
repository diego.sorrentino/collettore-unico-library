<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use CollettoreUnico\Division\Division as CU_Division;
use CollettoreUnico\Division\Struct as CU_DS_Division;

class DivisionTest extends TestCase{

	public function testFetch(): void {
		$this->obj = new CU_Division( 
				getenv('COLLETTORE_PROTOCOL'),
				getenv('COLLETTORE_FQDN'),
				intval(getenv('COLLETTORE_PORT')),
				getenv('COLLETTORE_AUTHCODE'));
			
		$this->assertLessThan ( $this->obj->fetch(), 1 );
	}

}

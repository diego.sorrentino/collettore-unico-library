<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use CollettoreUnico\Lock as CU_Lock;

class LockTest extends TestCase{

	public function testAcquirerRelease(): void {
		$this->obj = new CU_Lock( 
				getenv('COLLETTORE_PROTOCOL'),
				getenv('COLLETTORE_FQDN'),
				intval(getenv('COLLETTORE_PORT')),
				getenv('COLLETTORE_AUTHCODE'));

		$this->assertTrue( $this->obj->acquire() );

		$this->obj->release();
	}

}

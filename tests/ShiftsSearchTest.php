<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use CollettoreUnico\Shift\Search as CU_Shifts_Search;
use CollettoreUnico\Lock as CU_Lock;


class ShiftsSearchTest extends TestCase{
	private $obj; 

	public function testShiftSearchByDate(): void {
		$this->lockObj = new CU_Lock(
				getenv('COLLETTORE_PROTOCOL'),
				getenv('COLLETTORE_FQDN'),
				intval(getenv('COLLETTORE_PORT')),
				getenv('COLLETTORE_AUTHCODE'));
		$this->lockObj->acquire();

		$this->obj = new CU_Shifts_Search(
				getenv('COLLETTORE_PROTOCOL'),
				getenv('COLLETTORE_FQDN'),
				intval(getenv('COLLETTORE_PORT')),
				getenv('COLLETTORE_AUTHCODE'));
		$this->obj->addFilterByStartDate(new \Datetime('2024-12-12'));
		$this->obj->addFilterByStopDate(new \Datetime('2024-12-12'));

		$this->assertGreaterThan(-100, $this->obj->fetch());	

		$this->lockObj->release();
	}
}


<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use CollettoreUnico\TypeShift\TypeShift as CU_TypeShift;
use CollettoreUnico\TypeShift\Struct as CU_DS_TypeShift;


class TypeShiftTest extends TestCase{

	public function testFetch(): void {
		$this->obj = new CU_TypeShift( 
				getenv('COLLETTORE_PROTOCOL'),
				getenv('COLLETTORE_FQDN'),
				intval(getenv('COLLETTORE_PORT')),
				getenv('COLLETTORE_AUTHCODE'));

		$this->assertLessThan( $this->obj->fetch(), 1 );

		foreach($this->obj as $typeShift){
			$this->assertInstanceOf("\\CollettoreUnico\\TypeShift\\Struct", $typeShift);
		}
	}

}
